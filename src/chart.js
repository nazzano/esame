import Chart from "chart.js/auto";

// Create the chart taking from html the id
const ctx = document.getElementById("myChart").getContext("2d");

// Create two empty arrays
const data = [];
const labels = [];

// Create the chart
const chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels,
        datasets: [{
            label: 'Esame',
            data,
            fill: false,
            borderColor: "rgb(75, 192, 192)",
            tension: 0.1
        }],
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

// Construct the WebSocket using the WebSocket() constructor so that it returns a WebSocket
// object just created
const sck = new WebSocket("ws://localhost:9000");

// Listen to the message event to hear the message received from the beckend
sck.addEventListener("message", ev => {
    // Convert the values in an array of strings using with JSON.parse 
    const gf = JSON.parse(ev.data);

    // Convert "value" from string tu number
    // put "value" which represents the y-axis, and "time" which represents
    // the x-axis in the constants "date" and "labels"
    data.push(+gf.value);
    labels.push(gf.time);
});

// Execute a piece of code with a fixed time interval 
setInterval(() => {
    data,
    labels,
    // Update the chart
    chart.update();
}, 1000)